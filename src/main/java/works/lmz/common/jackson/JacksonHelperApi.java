package works.lmz.common.jackson;

/**
 * This exists to provide applications which need JSON serialization/deserialization
 * capability without being tied to a specific JSON implementation.
 */
public interface JacksonHelperApi {

	/**
	 * Serialises a provided Object to JSON.
	 *
	 * @param dataToSerialize The Object to serialise.
	 * @return The provided Object structure as a JSON String.
	 * @throws JacksonException
	 */
	String jsonSerialize(Object dataToSerialize) throws JacksonException;

	/**
	 * Constructs an Object from JSON String data.
	 *
	 * @param text The JSON String to parse.
	 * @param type The Class of the Object to construct.
	 * @param <T>  The type of Object to construct.
	 * @return The constructed Object if the JSON String input parses correctly.
	 * @throws JacksonException
	 */
	<T> T jsonDeserialize(String text, Class<T> type) throws JacksonException;
}
